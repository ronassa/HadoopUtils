import string
from timer import Timer


def collect_data():
	sqlite_conn = sqlite3.connect('bench.db')
	sqlite_cursor = sqlite_conn.cursor()
	sqlite_cursor.execute("SELECT field1, field2, field3, field4 FROM test LIMIT 100000")
	sqlite_resultSet = sqlite_cursor.fetchall()
	sqlite_conn.close()
	return sqlite_resultSet



def profile_line(msec,sec,rows):
	sec = round(t.secs,2)
	msec = t.msecs
	rate = round(msec / rows * 100,2)
	pline = str(rows)+ " records collected from sqlite db in: %s seconds (rate %s msec. per 100 rows)" % (sec,rate)
	return pline


with Timer() as t:
	sqlite_data = collect_data()
	rows_collected = len(sqlite_data)
print profile_line('collect_data',t.msecs,t.secs,rows_collected)
